package yuda.ichbal.coba1

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_siswa.*
import kotlinx.android.synthetic.main.frag_data_siswa.view.*

class FragmentSiswa : Fragment(),View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter.getItem(position) as Cursor
        namaKelas = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteSiswa->{

            }
            R.id.btnUpdateSiswa->{

            }
            R.id.btnInsertSiswa->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnCari->{
                showDataSiswa(edNamaSiswa.text.toString())
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    var namaSiswa : String = ""
    var namaKelas : String = ""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_siswa,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteSiswa.setOnClickListener(this)
        v.btnUpdateSiswa.setOnClickListener(this)
        v.btnInsertSiswa.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.lsSiswa.setOnItemClickListener(itemClicked)
        v.btnCari.setOnClickListener(this)

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataSiswa("")
        showDataKelas()
    }

    fun showDataSiswa(namaSiswa : String){
        var sql=""
        if(!namaSiswa.trim().equals("")){
            sql="select m.nis as _id, m.nama, p.nama_kelas from siswa m, kelas p " +
                    "where m.id_kelas=p.id_kelas and m.nama like '%$namaSiswa%'"
        }else{
            sql="select m.nis as _id, m.nama, p.nama_kelas from siswa m, kelas p " +
                    "where m.id_kelas=p.id_kelas order by m.nama asc"
        }
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_siswa,c,
            arrayOf("_id","nama","nama_kelas"), intArrayOf(R.id.txNisSiswa,R.id.txNamaSiswa,R.id.txNamaPS),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsSiswa.adapter = lsAdapter
    }

    fun showDataKelas(){
        val c : Cursor = db.rawQuery("select nama_kelas as _id from kelas order by nama_kelas asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    fun insertDataSiswa(nis : String, namaSiswa : String, id_kelas : Int){
        var sql = "insert into siswa(nis, nama, id_kelas) values(?,?,?)"
        db.execSQL(sql, arrayOf(nis,namaSiswa,id_kelas))
        showDataSiswa("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql ="select id_kelas from kelas where nama_kelas='$namaKelas'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            insertDataSiswa(v.edNisSiswa.text.toString(),v.edNamaSiswa.text.toString(),
                c.getInt(c.getColumnIndex("id_kelas")))
            v.edNisSiswa.setText("")
            v.edNamaSiswa.setText("")
        }
    }

    val itemClicked = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        v.edNamaSiswa.setText(c.getString(c.getColumnIndex("nama")))
        v.edNisSiswa.setText(c.getString(c.getColumnIndex("_id")))
    }
}

