package yuda.ichbal.coba1

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_alasan.*
import kotlinx.android.synthetic.main.frag_data_alasan.view.*

class FragmentAlasan : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertAlasan-> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var spAdapter1 : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var db : SQLiteDatabase
    var namaSiswa : String = ""
    var namaHari: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_alasan,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnInsertAlasan.setOnClickListener(this)
        v.spSiswa.onItemSelectedListener = this
        v.spHari.onItemSelectedListener = this
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataAlasan()
        showDataHari()
        showDataSiswa()
    }

    fun showDataAlasan(){
        var sql= "select n.id_alasan as _id, m.nama, k.nhari, n.alasan from alasan n join siswa m on n.nis = m.nis join hari k on n.kode = k.kode order by n.id_alasan asc"
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_alasan,c,
            arrayOf("_id","nama","nhari","alasan"), intArrayOf(R.id.txIdAlasan,R.id.txSiswa,R.id.txHari,R.id.txAlasan),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsAlasan.adapter = lsAdapter
    }

    fun showDataSiswa(){
        val c : Cursor = db.rawQuery("select nama as _id from siswa order by nama asc",null)
        spAdapter1 = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spSiswa.adapter = spAdapter1
        v.spSiswa.setSelection(0)
    }

    fun showDataHari(){
        val c1 : Cursor = db.rawQuery("select nhari as _id from hari order by nhari asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c1,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spHari.adapter = spAdapter
        v.spHari.setSelection(0)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spHari.setSelection(0,true)
        spSiswa.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter1.getItem(position) as Cursor
        val c1 = spAdapter.getItem(position) as Cursor
        namaHari = c1.getString(c.getColumnIndex("_id"))
        namaSiswa = c.getString(c.getColumnIndex("_id"))
    }

    fun insertDataAlasan(nama : String,nhari : String, alasan : String){
        var sql = "insert into alasan( nis, kode, alasan ) values (?,?,?)"
        db.execSQL(sql, arrayOf(nama,nhari,alasan))
        showDataAlasan()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select nis from siswa where nama='$namaSiswa'"
        var sql1 = "select kode from hari where nhari='$namaHari'"
        var c : Cursor = db.rawQuery(sql,null)
        var c1 : Cursor = db.rawQuery(sql1,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataAlasan(c.getString(c.getColumnIndex("nis")),c1.getString(c1.getColumnIndex("kode")),v.edAlasan.text.toString())
            v.edAlasan.setText("")

        }
        var x = v.edAlasan.text.toString()
        v.edAlasan.setText("")
    }
}