package yuda.ichbal.coba1

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener{

    lateinit var db : SQLiteDatabase
    lateinit var fragKelas : FragmentKelas
    lateinit var fragSiswa : FragmentSiswa
    lateinit var fragHari : FragmentHari
    lateinit var fragAlasan : FragmentAlasan
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragKelas = FragmentKelas()
        fragSiswa = FragmentSiswa()
        fragHari = FragmentHari()
        fragAlasan = FragmentAlasan()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemKelas ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragKelas).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemSiswa ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragSiswa).commit()
                frameLayout.setBackgroundColor(Color.argb(245,225,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAlasan ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragAlasan).commit()
                frameLayout.setBackgroundColor(Color.argb(245,225,225,225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemHari ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragHari).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,225,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout ->frameLayout.visibility = View.GONE
        }
        return true
    }
}
