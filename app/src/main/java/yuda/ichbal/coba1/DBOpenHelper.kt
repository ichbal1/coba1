package yuda.ichbal.coba1

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tHari = "create table hari(kode text primary key, nhari text not null, id_kelas int not null)"
        val tSiswa = "create table siswa(nis text primary key, nama text not null, id_kelas int not null)"
        val tAlasan = "create table alasan(id_alasan integer primary key autoincrement, nis text not null, kode text not null, alasan text not null)"
        val tKelas = "create table kelas(id_kelas integer primary key autoincrement, nama_kelas text not null)"
        val insKelas = "insert into kelas(nama_kelas) values('XII-IPA1'),('XII-IPA2'),('XII-IPA3')"
        db?.execSQL(tHari)
        db?.execSQL(tSiswa)
        db?.execSQL(tKelas)
        db?.execSQL(tAlasan)
        db?.execSQL(insKelas)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        val DB_Name = "absen"
        val DB_Ver = 1
    }
}